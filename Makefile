all: bib tex
	pdflatex vulnerability_metrics.tex
	evince vulnerability_metrics.pdf &

bib: vulnerability_metrics.bib
	pdflatex vulnerability_metrics.tex
	bibtex vulnerability_metrics

tex: vulnerability_metrics.tex
	pdflatex vulnerability_metrics.tex

clean:
	rm *.bbl *.aux *.blg *.log *.pdf
